from django.conf.urls import patterns, url
from mezzanine.core.views import direct_to_template

urlpatterns = patterns('',
	# url("^$", direct_to_template, {"template": "pages/menu.html"}, name="menu"),
    url(r'^partials/menu/(\d{1,4})/$', 'bmd_cms.menu.views.menu_item_modal'),
    url(r'^partials/special/(\d{1,4})/$', 'bmd_cms.menu.views.special_modal'),
)