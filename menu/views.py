from django.shortcuts import render_to_response
from .models import MenuItem, Special

def menu_item_modal(request, item_id):
	item = MenuItem.objects.get(pk=item_id)
	return render_to_response('partials/menuitem_modal.html', {"item": item, "imgsize": "600x400"})

def special_modal(request, item_id):
	item = Special.objects.get(pk=item_id)
	return render_to_response('partials/menuitem_modal.html', {"item": item, "imgsize": "600x400"})