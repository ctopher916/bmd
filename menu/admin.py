from copy import deepcopy
from django.contrib import admin
from mezzanine.pages.admin import PageAdmin
from mezzanine.core.admin import DisplayableAdmin, OwnableAdmin
from .models import Menu, MenuItem, MenuItemPrice, MenuItemCategory, Special

from django.utils.translation import ugettext_lazy as _


class MenuPricesInline(admin.TabularInline):
    """
    Admin class for menu item prices
    """
    model = MenuItemPrice


menuitem_fieldsets = deepcopy(DisplayableAdmin.fieldsets)
menuitem_fieldsets[0][1]["fields"].insert(1, "category")
menuitem_fieldsets[0][1]["fields"].extend(["content", "image", "order", "signature_item", "category_feature"])
menuitem_fieldsets = list(menuitem_fieldsets)
menuitem_list_filter = deepcopy(DisplayableAdmin.list_filter) + ("category", "signature_item")

class MenuItemAdmin(DisplayableAdmin):
    """
    Admin class for blog posts.
    """

    fieldsets = menuitem_fieldsets
    list_display = ["title", "status", "category", "order", "signature_item", "category_feature"]
    list_filter = menuitem_list_filter
    list_editable = ["status", "category", "order", "signature_item", "category_feature"]
    inlines = (MenuPricesInline,)
    # filter_horizontal = ("category", )


special_fieldsets = deepcopy(DisplayableAdmin.fieldsets)
special_fieldsets[0][1]["fields"].extend(["content", "price", "unit", "item", "image", "order"])
special_fieldsets = list(special_fieldsets)
special_list_filter = deepcopy(DisplayableAdmin.list_filter)

class SpecialAdmin(DisplayableAdmin):
    """
    Admin class for specials.
    """

    fieldsets = special_fieldsets
    list_display = ["title", "status", "price", "order"]    
    list_filter = special_list_filter
    list_editable = ["status", "price", "order"]



class MenuCategoryAdmin(admin.ModelAdmin):
    """
    Admin class for categories.
    """

    list_display = ["name", "order"]    
    list_editable = ["order"]

# class BlogCategoryAdmin(admin.ModelAdmin):
#     """
#     Admin class for blog categories. Hides itself from the admin menu
#     unless explicitly specified.
#     """

#     fieldsets = ((None, {"fields": ("title",)}),)

#     def in_menu(self):
#         """
#         Hide from the admin menu unless explicitly set in ``ADMIN_MENU_ORDER``.
#         """
#         for (name, items) in settings.ADMIN_MENU_ORDER:
#             if "blog.BlogCategory" in items:
#                 return True
#         return False


    

class MenuAdmin(PageAdmin):
    """Must register the 'fake' content type in the admin"""
    fieldsets = deepcopy(PageAdmin.fieldsets) # + author_extra_fieldsets


admin.site.register(Menu, MenuAdmin)
admin.site.register(MenuItem, MenuItemAdmin)
admin.site.register(MenuItemCategory, MenuCategoryAdmin)
admin.site.register(Special, SpecialAdmin)
