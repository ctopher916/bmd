from mezzanine.pages.page_processors import processor_for
from mezzanine.pages.models import Page
from mezzanine.core.models import CONTENT_STATUS_PUBLISHED
from django.conf import settings
from .models import Menu, MenuItem, MenuItemCategory, Special

def fileExists(path):
   from os.path import exists
   path = settings.MEDIA_ROOT+'/'+path
   return path and exists(path)

@processor_for("menu")
def active_items(request, page):
    menu_categories = MenuItemCategory.objects.all()
    # active_items = MenuItem.objects.filter(status=CONTENT_STATUS_PUBLISHED)
    categorized_items = []
    for cat in menu_categories:
        items = MenuItem.objects.filter(status=CONTENT_STATUS_PUBLISHED, category=cat)
        categorized_items.append({'cat':cat,'items':items})

    return {"categorized_items":categorized_items}

@processor_for("menu")
def menu_downloads(request, page):

    items = [
        {"title": "print menu", "icon_class":"icon-print", "file": "uploads/menu/bake_my_day_menu.pdf"},
        {"title": "daily lunch menu", "icon_class":"icon-download", "file": "uploads/menu/bake_my_day_daily_lunch_menu.pdf"},
    ]
    download_items = []
    for item in items:
        if fileExists(item["file"]):
            item['url'] = settings.MEDIA_URL + item["file"]
            download_items.append(item)
    return {"download_items":download_items}

@processor_for("/")
def featured_items(request, page):
    featured_items = MenuItem.objects.filter(category_feature=True)
    return {"featured_items":featured_items}

# @processor_for("menu")
# def specials(request, page):
#     specials = Special.objects.filter(status=CONTENT_STATUS_PUBLISHED)
#     # for item in items:
#     #     specials.append(item.item)
#     return {"specials": specials}
