from mezzanine.pages.models import Page
from mezzanine.blog.models import BlogPost
from mezzanine.core.models import CONTENT_STATUS_PUBLISHED
from mezzanine.conf import settings
from bmd_cms.menu.models import Special, MenuItem

import re
import json

def clean_phone(ph_num):
    """leave only digits"""
    return re.sub(r"\D", "", ph_num)

def slug_exists(slug):
    # True if there is a page with this slug AND it is published
    try:
        return bool(Page.objects.get(slug=slug, status=CONTENT_STATUS_PUBLISHED))
    except:
        return False

def have_objects(Obj):
    return Obj.objects.filter(status=CONTENT_STATUS_PUBLISHED).exists()

def get_main_menu():
    main_menu = [
        {"href": "#body", "txt": "Home", "title": "BMD Home", "display": True},
        {"href":"#specials", "txt": "Specials", "title": "Check out our awesome specials", "display": have_objects(Special)},
        {"href":"/menu", "txt": "Our Menu", "title": "Our Menu", "display": have_objects(MenuItem)},
        {"href":"/gallery", "txt": "Photos", "title": "Photo gallery", "display": slug_exists('gallery')},
        # {"href":"/gallery", "txt": "Photos", "title": "Photo gallery", "display": False},
        {"href":"#about", "txt": "About Us", "title": "Who we are", "display": slug_exists('about')},
        {"href":"/blog", "txt": "News", "title": "News", "display": have_objects(BlogPost)},
        {"href":"/faqs", "txt": "FAQs", "title": "FAQs", "display": slug_exists('faqs')},
        {"href":"#contact", "txt": "Contact", "title": "Get in touch", "display": slug_exists('contact')}
    ]
    return [item for item in main_menu if item['display']]

def parse_hours(hours_string):
    # format is: "1 11:00-17:00,2 10:00-17:00, 3 10:00-17:00, 4 10:00-17:00, 5 10:00-17:00"
    store_hours = {}
    daily_hours = hours_string.split(",")
    daily_hours = [s.strip() for s in daily_hours]
    for h in daily_hours:
        day_num, hours = h.split()
        openh,closeh = hours.split("-")
        store_hours[day_num] = (openh,closeh)
    return json.dumps(store_hours)

def bmd_common(request):
    import time
    return {"bmd_phone": (settings.BMD_PHONE_MAIN, clean_phone(settings.BMD_PHONE_MAIN)),
        "bmd_fax": (settings.BMD_PHONE_FAX, clean_phone(settings.BMD_PHONE_FAX)),
        "bmd_email": settings.BMD_EMAIL,
        "bmd_fb_url": settings.BMD_FB_URL,
        "bmd_tw_url": settings.BMD_TW_URL,
        "bmd_instagram_url": settings.BMD_INSTAGRAM_URL,
        "bmd_gp_url": settings.BMD_GP_URL,
        "bmd_hours": parse_hours(settings.BMD_HOURS),
        "bmd_hours_prose": settings.BMD_HOURS_PROSE,
        "bmd_main_menu": get_main_menu(),
        "num_home_features": settings.BMD_NUM_HP_FEATURES,
        "bmd_hp_carousel_ms": settings.BMD_HP_CAROUSEL_MS,
        "bmd_menu_item_truncate": settings.BMD_MENU_BLURB_TRUNCATE_WORDS,
        "this_year": time.strftime("%Y")
    }