from django.db import models
from mezzanine.pages.models import Page
from mezzanine.core.models import RichText

# class FauxPage(Page, RichText):
#     """This fake model is necessary to create a Mezzanine type, 
#         even if it really doesn't do anything"""
#     pass

# class HomePage(Page, RichText):
#     '''
#     A page representing the format of the home page
#     '''
#     page_title = models.CharField(max_length=200,
#         help_text="The main heading",
#         default="Made from scratch every day")
#     menu_teaser_button_text = models.CharField(max_length=100,
#         help_text="The button below the menu teaser",
#         default="Our full menu")
#     home_gallery_button_text = models.CharField(max_length=100,
#         help_text="The button below the homepage gallery",
#         default="Gallery")
#     specials_title = models.CharField(max_length=200,
#         help_text="The heading for the daily specials. Markdown allowed.",
#         default="What's on *special* today?")
#     blog_title = models.CharField(max_length=200,
#         help_text="The heading for the news section. Markdown allowed.",
#         default="What we're up to")
#     blog_button_text = models.CharField(max_length=100,
#         help_text="The button below the news section",
#         default="News")
#     contact_title = models.CharField(max_length=200,
#         help_text="The heading for the main contact section. Markdown allowed.",
#         default="Get in touch and stay updated")
#     contact_form_title = models.CharField(max_length=200,
#         help_text="The heading for the contact form. Markdown allowed.",
#         default="Send us a note")
#     contact_form_button_text = models.CharField(max_length=100,
#         help_text="The submit button for the contact form",
#         default="Send it")
#     map_title = models.CharField(max_length=200,
#         help_text="The heading for the map. Markdown allowed.",
#         default="Find us in the real world")
#     contact_info_title = models.CharField(max_length=200,
#         help_text="The heading for contact info. Markdown allowed.",
#         default="Get in touch")
#     hours_title = models.CharField(max_length=200,
#         help_text="The heading for hours listing. Markdown allowed.",
#         default="Our hours")
#     subscribe_title = models.CharField(max_length=200,
#         help_text="The heading for subscription form. Markdown allowed.",
#         default="Be fresh, subscribe to get our latest news")
#     subscribe_button_text = models.CharField(max_length=100,
#         help_text="The submit button for the subscription form",
#         default="Subscribe")

#     class Meta:
#         verbose_name = "Home page"
#         verbose_name_plural = "Home pages"