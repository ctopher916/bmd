$(document).ready(function() {

  var getMinuteOffsetFromMidnight = function (h, m) {
    return h * 60 + m;
  }

  var timeFromStr = function (str) {
    t = str.split(":");
    return [parseInt(t[0]), parseInt(t[1])];
  }

  setOpen = function () {
    $('.hours .status').addClass("hide");
    $('.hours .open').removeClass("hide");
  }

  setClosed = function () {
    $('.hours .status').addClass("hide");
    $('.hours .closed').removeClass("hide");
  }

  checkIfOpen = function () {
    var d = new Date();
    //d.setHours(10);
    
    var day = d.getDay();
    var h = d.getHours();
    var m = d.getMinutes();

    var hours = $('.hours').data('hours');
    var today = hours[d.getDay()];
        
    if (typeof today === 'undefined') {
      setClosed();
      return;
    }
        
    var now = getMinuteOffsetFromMidnight(h,m);
    var open = timeFromStr(today[0]);
    open = getMinuteOffsetFromMidnight(open[0], open[1]);
    var close = timeFromStr(today[1]);
    close = getMinuteOffsetFromMidnight(close[0], close[1]);
        
   //console.log(now, open, close);
        
    if (now > open && now < close) {
      setOpen();
    } else {
      setClosed();
    }
  } 

  checkIfOpen();
  setInterval(checkIfOpen, 1000 * 60 * 1);    // every 1 minute
  
});