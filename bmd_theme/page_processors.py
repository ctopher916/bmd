from mezzanine.pages.page_processors import processor_for
from mezzanine.pages.models import Page
from mezzanine.blog.models import BlogPost
from mezzanine.forms.page_processors import form_processor
from mezzanine.core.models import CONTENT_STATUS_PUBLISHED
from mezzanine.conf import settings
from bmd_cms.menu.models import Special

from mezzanine.conf import settings

@processor_for("/")
def is_home(request, page):
    is_home = False
    if page.slug == "/":
        is_home = True
    return {"is_home": is_home}

@processor_for("/")
def page_content(request, page):
    about_page = contact_page = contact_form = carousel_gallery = teaser_gallery = None
    try:
        about_page = Page.objects.get(slug='about')
    except:
        pass
    try:
        contact_page = Page.objects.get(slug='contact')
    except:
        pass
    try:
        contact_form = form_processor(request, contact_page)
    except:
        pass
    try:
        slug = settings.BMD_GALLERY_CAROUSEL_SLUG
        carousel_gallery = Page.objects.get(slug=slug)
    except:
        pass
    try:
        slug = settings.BMD_GALLERY_TEASER_SLUG
        teaser_gallery = Page.objects.get(slug=slug)
    except:
        pass
    return {"about_page": about_page,
        "contact_page": contact_page,
        "contact_form": contact_form,
        "carousel_gallery": carousel_gallery,
        "teaser_gallery": teaser_gallery}

@processor_for("menu")
@processor_for("/")
def specials(request, page):
    items = list(Special.objects.filter(status=CONTENT_STATUS_PUBLISHED))

    for item in items:
        # attributes defined on the special override attributes
        # defined on the menu item
        item.title = item.title or item.item.title
        item.content = item.content or item.item.content
        item.description = item.description or item.item.description
        item.image = item.image or item.item.image
        item.price = item.price or item.item.price

    return {"specials": items}

@processor_for("/")
def posts(request, page):
    return {"posts": BlogPost.objects.filter(status=CONTENT_STATUS_PUBLISHED).order_by('-publish_date')[:2]}