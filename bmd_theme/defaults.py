from mezzanine.conf import register_setting

register_setting(
    name="BMD_PHONE_MAIN",
    description="The main phone number.",
    editable=True,
    default=""
)

register_setting(
    name="BMD_PHONE_FAX",
    description="The main fax number.",
    editable=True,
    default=""
)

register_setting(
    name="BMD_EMAIL",
    description="The main email address.",
    editable=True,
    default=""
)

register_setting(
	name="BMD_HOURS",
    description="Shop's open hours. 24 hour time format. Monday = 1.",
    editable=True,
    default="1 11:00-17:00, 2 10:00-17:00, 3 10:00-17:00, 4 10:00-17:00, 5 10:00-17:00"
)

register_setting(
    name="BMD_HOURS_PROSE",
    description="Shop's open hours. Human readable version. Use &lt;br&gt; to break lines.",
    editable=True,
    default="Mon 11:00am - 5:00pm<br>Tue - Fri: 10:00am - 5:00pm"
)

register_setting(
    name="BMD_FB_URL",
    description="Facebook URL",
    editable=True,
    default=""
)

register_setting(
    name="BMD_TW_URL",
    description="Twitter URL",
    editable=True,
    default=""
)

register_setting(
    name="BMD_INSTAGRAM_URL",
    description="Instragram URL",
    editable=True,
    default=""
)

register_setting(
    name="BMD_GP_URL",
    description="Google+ URL",
    editable=True,
    default=""
)

register_setting(
    name="BMD_NUM_HP_FEATURES",
    description="How many featured items to show on homepage",
    editable=True,
    default="5"
)

register_setting(
    name="BMD_GALLERY_CAROUSEL_SLUG",
    description="Gallery to use for homepage carousel. Slug is the last part of the URL. http://bakemydaymobile.com/sample-gallery/ = 'sample-gallery'",
    editable=True,
    default="sample-gallery"
)

register_setting(
    name="BMD_GALLERY_TEASER_SLUG",
    description="Gallery to use for homepage carousel. Slug is the last part of the URL. http://bakemydaymobile.com/sample-gallery/ = 'sample-gallery'",
    editable=True,
    default="sample-gallery"
)

register_setting(
    name="BMD_HP_CAROUSEL_MS",
    description="Timing for the homepage carousel, in ms",
    editable=True,
    default="3000"
)

register_setting(
    name="BMD_MENU_BLURB_TRUNCATE_WORDS",
    description="The number of words to display in the shortened menu item description",
    editable=True,
    default="10"
)
