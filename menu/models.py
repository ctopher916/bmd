from django.db import models
from mezzanine.pages.models import Page
from mezzanine.core.models import MetaData, Displayable, Orderable, RichText
from mezzanine.core.fields import RichTextField
from datetime import datetime
from django.conf import settings


class Menu(Page, RichText):
	"""This fake model is necessary to create a Mezzanine type, 
		even if it doesn't really do anything"""
	pass


class MenuItemCategory(models.Model):
	name = models.CharField(max_length=100)
	order = models.IntegerField(default=0, 
		help_text="Any positive or negative integer. Specials will always be shown first.")

	class Meta:
		verbose_name_plural = "Menu Item Categories"
		ordering = ['order', 'name']

	def __unicode__(self):
		return self.name


class MenuItem(Displayable, RichText):
	category = models.ForeignKey(MenuItemCategory)
	signature_item = models.BooleanField(help_text="Is this a BMD Signature Item?", default=False)
	category_feature = models.BooleanField(help_text="Should this be the featured item in it's category?", default=False)
	image = models.ImageField(blank=True, 
		null=True, 
		upload_to='images/menu')
	order = models.IntegerField(max_length=5,
		default=0,
		help_text="Any positive or negative integer")

	class Meta:
		ordering = ['category', '-signature_item', 'order', 'title']

	def __unicode__(self):
		return "%s [%s]" % (self.title, self.category.name)

	def get_absolute_url(self):
		return "/menuitem/%s" % self.id

	@property
	def get_image(self):
		if self.image:
			return "%s%s" % (settings.MEDIA_URL, self.image)
		else:
			return "/static/common/images/no-image.png"

	@property
	def image_absolute_url(self):
		return "%s%s" % (settings.MEDIA_URL, self.image)


class MenuItemPrice(models.Model):
	desc = models.CharField(max_length=100,
		blank=True)
	item = models.ForeignKey(MenuItem)
	price = models.FloatField(blank=True, 
		null=True)
	unit = models.CharField(max_length=10, 
		blank=True, 
		default="ea.")
	order = models.IntegerField(max_length=5,
		default=0,
		help_text="Any positive or negative integer. Leave blank to sort by ascending price (this is probably what you want anyway)")

	class Meta:
		verbose_name_plural = "Menu Item Prices"
		ordering = ['order', 'price']

	def __unicode__(self):
		return "%s %s" % (self.price, self.unit)


class Special(Displayable, RichText):
	item = models.ForeignKey(MenuItem)
	image = models.ImageField(blank=True, 
		null=True, 
		upload_to='images/specials',
		help_text="The item image will be used if this is left blank")
	price = models.FloatField(blank=True, 
		null=True,
		help_text="Use only for item-specific specials")
	unit = models.CharField(max_length=10, 
		blank=True, 
		default="ea.")
	order = models.IntegerField(max_length=5,
		default=0,
		help_text="Any positive or negative integer")

	class Meta:
		ordering = ['order', 'title']

	def __unicode__(self):
		return self.title

	def get_absolute_url(self):
		return "/special/%s" % self.id

	@property
	def get_image(self):
		return self.image or self.item.image